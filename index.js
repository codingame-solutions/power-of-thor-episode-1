//https://www.codingame.com/ide/puzzle/power-of-thor-episode-1

var inputs = readline().split(' ');
const lightX = parseInt(inputs[0]); // the X position of the light of power
const lightY = parseInt(inputs[1]); // the Y position of the light of power
const initialTX = parseInt(inputs[2]); // Thor's starting X position
const initialTY = parseInt(inputs[3]); // Thor's starting Y position

var x = initialTX;
var y = initialTY;
// game loop
while (true) {
    const remainingTurns = parseInt(readline()); // The remaining amount of turns Thor can move. Do not remove this line.
    var directionX = "";
    var directionY = "";

    if (x > lightX) {
        directionX = "W";
        x--;
    } else if ( x < lightX) {
        directionX = "E";
        x++;
    }

    if (y > lightY) {
        directionY = "N"
        y--;
    } else if (y < lightY) {
        directionY = "S";
        y++;
    }
    console.log(directionY + directionX);
    // A single line providing the move to be made: N NE E SE S SW W or NW

}
